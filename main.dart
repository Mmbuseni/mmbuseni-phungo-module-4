import 'package:flutter/material.dart';
import 'package:mmbuseni_phungo_module3/splashscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: '',
      home: const SplashAnimation(),
      theme: ThemeData(
          primarySwatch: Colors.purple, accentColor: Colors.purple[50]),
    );
  }
}
