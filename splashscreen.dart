import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:mmbuseni_phungo_module3/login.dart';

class SplashAnimation extends StatelessWidget {
  const SplashAnimation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splash: Image.asset('assets/splashimage.jpg'),
        nextScreen: const LoginPage(),
        splashTransition: SplashTransition.fadeTransition);
  }
}
